FROM yiisoftware/yii2-php:7.3-fpm

WORKDIR /app/web
RUN composer create-project --prefer-dist yiisoft/yii2-app-basic .
RUN composer require --prefer-dist proget-hq/phpstan-yii2
COPY phpstan.neon /app/web/config/phpstan.neon


RUN vendor/bin/phpstan analyse -c config/phpstan.neon models/ --level 0